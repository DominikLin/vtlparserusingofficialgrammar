# README #

### Purpose of this repository ###

Project to set up parser, lexer for official VTL grammar (v1.1.0, see https://github.com/vtl-sdmx-task-force/sdmx-vtl) in order to test examples from the manual.

### How do I get set up? ###

1. Clone this repository
2. configure the string *expression* in the class *OfficialVtlParser*
3. run the **main()** method of the class *OfficialVtlParser*
4. the abstract syntax tree of the given input will be displayed on the console

### Contact ###

dominiklin85@gmail.com