package ecb.officialVtlParser;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import antlr4.VtlLexer;
import antlr4.VtlParser;
import ecb.officialVtlParser.abstractClasses.AbstractCompiler;

public class OfficialVtlCompiler extends AbstractCompiler {
    @Override
    public ParseTree compile(String expression) {
	ANTLRInputStream input = new ANTLRInputStream(expression);
	VtlLexer lexer = new VtlLexer(input);
	CommonTokenStream tokens = new CommonTokenStream(lexer);
	VtlParser parser = new VtlParser(tokens);
	ParseTree tree = parser.start();
	return tree;
    }
}
