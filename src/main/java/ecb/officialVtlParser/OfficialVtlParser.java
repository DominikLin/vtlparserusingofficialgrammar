package ecb.officialVtlParser;

import org.antlr.v4.runtime.tree.ParseTree;

import ecb.officialVtlParser.abstractClasses.AST;

public class OfficialVtlParser {
    public static void main(String[] args) {
	String expression = ""
		/* examples not covered by parser rules */		
//		+"/*UM, line 1737-1740*/define function compare_integer_descending(x as integer, y as integer) returns boolean as x > y"
//		+"/*UM, line 1784-1784*/define function has_solution(a, b, c) as b*b-4*a*c>0"
//		+"/*UM, line 1790-1792*/define function has_solution(a as number, b as number, c as number) returns boolean as b*b-4*a*c>0"
//		+"/*UM, line 1802-1806*/define function distance(d1 as dataset, d2 as dataset, n as integer := 2) as (d1^n - d2^n)^(1/n)"
//		+"/*UM, line 1813-1816*/define function z_transform(x as number, mu as number := 0, sigma as number := 1) as (x-mu)/sigma"
//		+"/*UM, line 1840-1845*/define procedure quot_rem(in ds as dataset, in divisor as number, out quot as dataset, out rem as dataset) as { quot := floor(ds / divisor) rem := ds - quot*divisor }"
//		+"/*UM, line 1852*/call quot_rem(PopPerCountry, AvgPop, Multiple, Remainder)"
//		+"/*UM, line 2085-2086*/type population = dataset { identifier geo as string identifier year as integer measure population as float attribute status as string }"		
//		+"/*RM, line 945-S947*/define datapoint ruleset ruleset_1 (FLOW as x, OBS_STATUS as y) (flow_dr : when x = \"CREDIT\" or x = \"AVERAGE\" then y <> \"C\" errorcode ( -XXXXX ))"
//		+"/*RM, line 951*/ds := check(ds_bop, ruleset_1, with measures , only failures)"
//		+"/*RM, line 1130-1135*/define hierarchical ruleset AddBenelux (valuedomain=GeoArea) is Belgium = Belgium Luxembourg = Luxembourg Netherlands = Netherlands Benelux = Belgium + Luxembourg + Netherlands end hierarchical ruleset"
//		+"/*RM, line 1259-1261*/define hierarchical ruleset vr_sex (Variable= sex) is TOTAL = MALE + FEMALE; end hierarchical ruleset"
//		+"/*RM, line 1265-1267*/define hierarchical ruleset BeneluxCountriesHierarchy ( valuedomain=Geo_Area) is BENELUX = BELGIUM + LUXEMBOURG + NETHERLANDS errorcode 2000 errorlevel 10 end hierarchical ruleset "
//		+"/*RM, line 1448*/ds_3 := ds_1.M1 + ds_2.M2 as \"M1\""
//		+"/*RM, line 1454*/ds_2 := ds_1.M1* 10 as \"M2\""
//		+"/*RM, line 1492*/alterDataset(ds_1 all)"
//		+"/*RM, line 1583-1584*/ds_1 := get(\"DF_NAME/2000-2010.USD.M.F.A.BOP.ANN.STO\", \"DF_NAME/2011-2012.USD.M.F.A.BOP.ANN.STO\", keep(K1, K2, K3, M1), dedup(M1*min))"
//		+"/*RM, line 1601-1602*/ds_1 := get(\"DF_NAME/2000-2010.USD.M.F.A.BOP.ANN.STO\", (\"DF_NAME/2011-2012.USD.M.F.A.BOP.ANN.STO\", keep(K1, K2, K3, M1), dedup(M1*min), filter(K3=\"X\")))"
//		+"/*RM, line 1616-1618*/ds_1 := get(\"DF_NAME/2000-2010.USD.M.F.A.BOP.ANN.STO\", \"DF_NAME/2011-2012.USD.M.F.A.BOP.ANN.STO\", keep(K1, K2, M1), dedup(M1*min), aggregate(sum(M1)))"
//		+"/*RM, line 1837-1840*/ds3 := [ds1, ds2] {obs_value = ds1.obs_value + ds2.obs_value, obs_status = ds1.obs_status}"
//		+"/*RM, line 1847-1850*/ds3 := {obs_value = ds1.obs_value + ds2.obs_value, obs_status = ds1.obs_status}"
//		+"/*RM, line 2150*/A := \"Hello\" || \", world! \""
//		+"/*RM, line 2153*/ds_r := ds_1 || ds_2"
//		+"/*RM, line 3439*/ds_r := match_characters(population.TIME,\"[123456789,]\",all)"
//		+"/*RM, line 3587*/ds_3:= func_dep ( ds_1, (FISCAL_CODE ), (NAME, DATE_OF_BIRTH,PLACE_OF_BIRTH))"
//		+"/*RM, line 3590*/ds_2 := func_dep ( NAME), (FISCAL_CODE) )"		
//		+"/*RM, line 3834*/B := string_from_date (A, YYYY-MM)"
//		+"/*RM, line 3837*/ds_2:= string_from_date (ds_1, \"YYYY-MM\")"
//		+"/*RM, line 3984*/d_r := symdiff(total_population1, total_population2)"
//		+"/*RM, line 4015*/d_r := setdiff ( total_population1,total_population2)"
//		+"/*RM, line 4020*/DatasetC := setdiff (DatasetA ,DatasetB)"
//		+"/*RM, line 4053*/ds_2 := ds_1 [ time = 2010, ref_area = EU25 ]"
//		+"/*RM, line 4058*/ds_2 := ds_1 [time = 2010, ref_area = EU25, partner = CA ]"
//		+"/*RM, line 4061*/ds_2 := ds_1 [ref_area = EU25 ] + ds_1[ ref_area = BG ] + ds_1 [ ref_area = RO ]"
//		+"/*RM, line 4064*/ds_2 := ds_1 [ time = 2010, ref_area = EU25 ]"
//		+"/*RM, line 4108*/ds_2 := transcode( ds, ds_map, REF_AREA )"
//		+"/*RM, line 4188*/ds_2 := aggregate( ds_bop, hr_ref_area );"
//		+"/*RM, line 4191*/ds_2 := aggregate( ds_bop, hr_ref_area, return all data points );"
//		+"/*RM, line 4201*/ds_2 := aggregate(ds_bop, hr_ref_area);"
//		+"/*RM, line 4213*/ds_2 := aggregate(IT_nord_bop, hr_IT_nord_pop);"
//		+"/*RM, line 4271*/ds_agg := avg ( ds_bop.obs_value ) group by time"
//		+"/*RM, line 4279*/ds_agg := avg ( ds_bop.obs_value ) group by time, ref_area"
//		+"/*RM, line 4284*/ds_agg := avg(ds_bop)"
//		+"/*RM, line 4287*/ds_agg := max ( ds_bop.obs_value ) as max_value, min ( ds_bop.obs_value ) as min_value group by time"
//		+"/*RM, line 4544*/ds_1 := ratio_to_report ( ds_bop ) over ( partition by REF_AREA, partner)"
//		+"/*RM, line 4707*/Income_by_country_and_nace_ISO := hierarchy(\"Income_by_country_and_nace_UN\", GEO, un_to_ISO_aggr, false)"
//		+"/*RM, line 4709-4710*/ncome_by_country_and_nace_ISO := hierarchy(\"Income_by_country_and_nace_UN\", GEO, ((\"ITA\",1,+)) to \"IT\", ((\"ALB\",1,+)) to AL, ((\"BEL\",1,+)) to \"BE\") as un_to_ISO_aggr, false)"
//		+"/*RM, line 4717*/Income_by_continent_and_nace := hierarchy(\"Income_by_state_and_nace\", GEO, Continent_aggr, false)"
//		+"/*RM, line 4726*/Income_by_world_and_nace := hierarchy(\"Income_by_state_and_nace\", GEO, World_aggr, false)"
//		+"/*RM, line 4737*/Income_world_and_nace_par := hierarchy(\"Income_by_state_and_nace\", World_aggr_par, false)"
//		+"/*RM, line 4739-4745*/Income_world_and_nace_par := hierarchy ( Income_by_state_and_nace, GEO, ((\"Italy\",1,+),(\"France\",1,+),(\"Luxembourg\",1,+) to \"Europe, ((\"China\",1,+),(\"India\",1,+)) to \"Asia\", ((\"USA\",1,+)) to \"America, ((\"Asia\",2,+),(\"Europe\",2,+),(\"America\",2,+), (\"Oceania\",2,+),(\"Africa\",2,+)) to \"World\") as World_aggr_par, false)"
//		+"/*RM, line 4757*/Italian_income_by_nace:= hierarchy(\"Income_by_state_and_nace\", Italy_filter, true)"
//		+"/*RM, line 4767*/income_prod_2:= hierarchy(\"Income_by_state_and_nace_mm\", GEO, mult_rule, false, prod)"
//		+"/*RM, line 4835-4843*/define datapoint ruleset dpr_ labour ( AGE as a) (rule_1 when a between 14 and 17 then edu <> 5 ;rule_2 when a between 16 and 19 then edu <> 6 ;rule_3 when a between 17 and 20 then edu <> 7 and edu <> 8 ;rule_4 when a between 18 and 21 then edu <> 10 ; rule_5 when a between 14 and 16 then edu <> 13 ;rule_6 when a between 16 and 18 then edu <> 14 ; rule_7 when a between 17 and 20 then edu <> 15 ;) ;"
//		+"/*RM, line 4845*/ds_validation_report := check ( ds_labour, dpr_labour, not valid, condition )"
//		+"/*RM, line 4974-4977*/ds_moving_average := avg(ds_bop) over (partition by ref_area, partner \n order by time \n rows between 1 preceding and 1 following);"
//		+"/*RM, line 4978*/ds_outliers := check ( abs ( ds_bop.obs_value – ds_moving_average.obs_value ) <= 10 ) ;"
//		+"/*RM, line 5039*/ds_1 := check_value_domain_subset (ds_1, l_1, vds_1)"
//		+"/*RM, line 5225*/ts_2 := timeshift( ts_1, A, 1 )"
//		+"/*RM, line 5227*/ts_2 := timeshift( ts_1, M,12 )"
//		+"/*RM, line 5228*/ts_2 := timeshift( ts_1, Q, 3 )"
//		+"/*RM, line 5461*/ds_1 := population1[keep SEX, GEO, POPULATION ]"	
//		+"/*RM, line 5513*/ds_2 := ds_1[calc M1*M2/3 as \"M4\" role MEASURE]"
//		+"/*RM, line 5516*/ds_2 := ds_1[calc M1-1 as \"M1\" role MEASURE, M1+M2 as M2, if M2>3 then M2 else M3 as M3]"
//		+"/*RM, line 5519*/ds_2 := ds_1[calc A1 + A2 as \"A3\" role ATTRIBUTE viral]"
//		+"/*RM, line 5562*/ds_2 := ds_1[attrcalc QUALITY+1 as QUALITY]"
//		+"/*RM, line 5567*/ds_r := (ds_1 + ds_2)[attrcalc ds_1.QUALITY + ds_2.QUALITY as QUALITY]"
//		+"/*RM, line 5576-5581*/ds_r := (ds_1 + ds_2) [attrcalc if ds_1.QUALITY=\"A\" and ds2.QUALITY=\"B\" then \"C\" elseif ds1.QUALITY=\"K\" and ds.2QUALITY=\"K\" then \"M\" else \"Z\" AS AGGREGATED_QUALITY]"
//		+"/*RM, line 5591*/ds_r := (ds_1 + ds_2)[attrcalc ds_1.QUALITY_1 + ds_2.QUALITY_1 as QUALITY_1, ds_2.QUALITY_2 as QUALITY_2]"
		;
	
	OfficialVtlCompiler compiler = new OfficialVtlCompiler();
	ParseTree tree = compiler.compile(expression);
	
	AST ast = new AST(tree);
	System.out.println(ast.toString());
    }
}
